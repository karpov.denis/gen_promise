const worker = (gen, ...args) => {
  let start = null;

  if ((typeof gen.constructor === 'function' &&
    /Generator/.test(gen[Symbol.toStringTag]))) {

    start = gen.apply(null, args);
  } else {

    if ((typeof gen.constructor === 'object' &&
      /Generator/.test(gen[Symbol.toStringTag]))) {
      start = gen;
    } else {
      throw new TypeError("First argument is not a Generator/Iterator")
    }

  }

  const flightControl = arg => {
    let x = start.next(arg);

    if (x.done) {
      return x.value;
    }

    if (typeof x.value === 'function') {
      let result = null;

      result = (typeof x.value().next === 'function') ?
        starter(x.value) : x.value()

      return flightControl(result);
    }

    if (typeof x.value === 'number') {
      return flightControl(x.value);
    }
    if (typeof x.value.then === 'function') {
      return x.value.then(flightControl);
    }

    if (/enerator/.test(x.value[Symbol.toStringTag])) {
      return flightControl(starter(x.value))
    }

  };

  return flightControl();
};

